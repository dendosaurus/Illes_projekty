#include <NewPing.h>
NewPing sonar(2, 3, 255);

void setup() {
  // put your setup code here, to run once:
  pinMode(4, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(29);
  byte cm = sonar.ping_cm();
  if (cm < 10 && cm != 0) {
    digitalWrite(4, 0);
  } else {
    digitalWrite(4, 1);
  }

}
