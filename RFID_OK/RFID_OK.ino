#include <SPI.h>
#include <RFID.h>
RFID one(10, 9);
RFID two(8, 7);
RFID three(5, 6);
RFID four (4, 3);
bool a = false;
bool b = false;
bool c = false;
bool d = false;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  SPI.begin();
  one.init();
  two.init();
  three.init();
  four.init();
  pinMode(2, OUTPUT);
  digitalWrite(2, 1);
  Serial.println("OK");
}

void loop() {
  // put your main code here, to run repeatedly
  if (a == 1 && b == 1 && c == 1 && d == 1) {
    digitalWrite(2, 0);
  } else {
    digitalWrite(2, 1);
  }
  if (one.isCard()) {

    if (one.readCardSerial()) {
      Serial.print(one.serNum[0]);
      if ( one.serNum[0] == 144) {
        a = true;
      }
    }
  } else {
    a = 0;
  }
  one.halt();

  if (two.isCard()) {
    if (two.readCardSerial()) {
      Serial.print(two.serNum[0]);
      if ( two.serNum[0] == 96) {
        b = true;
      }
    }
  } else {
    b = 0;
  }
  two.halt();

  if (three.isCard()) {
    if (three.readCardSerial()) {
      Serial.print(three.serNum[0]);
      if ( three.serNum[0] == 133) {
        c = true;
      }
    }
  } else {
    c = 0;
  }
  three.halt();

  if (four.isCard()) {
    if (four.readCardSerial()) {
      Serial.print(four.serNum[0]);
      if ( four.serNum[0] == 112) {
        d = true;
      }
    }
  } else {
    d = 0;
  }
  four.halt();
}
